#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/*
 *  
 *  [you may need add library header to use]
 * 
 */


#define MAX_SIZE_LENGTH 32

/****************************************
 * Purpose: Storing calculated value from function to buff pointer
 * Input:  int * pointer, char * input string
 *    Return:
 *        0: success
 *       -1: fail
 *      
 ****************************************/
int storeStringFromFunc(int **buff, char *input)
{
    int ret = -1;

    // do check input error:



    // assign memory to buff


    
    // initial pointer value

    
    // calculate value



    // store value to buff


    // if success calculated return ret = 0
    return ret;
}

int main(void){

    int *buff = NULL;
    char input1[MAX_SIZE_LENGTH] = "123456789abcdef";
    int ret = -1;

    ret = storeStringFromFunc(&buff, input1);

    // expect result show: 120, 0
    printf("result: %d, ret: %d\n", buff[0], ret);

    // release buff pointer


	return 0;
}
