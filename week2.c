#include <stdio.h>
#include <stdlib.h>
/*
 *  
 *  [you may need add library header to use]
 * 
 */
#include <string.h>

#define MAX_SIZE_LENGTH 128

/****************************************
 * Purpose: Calculate each char number value in input list function
 * Input: one char string with number list
 * Return:
 *        integer: result
 *      
 ****************************************/
int calculateString(char *full_string)
{
    int result = 0;
    int value = 0;
    char listNumber1[64] = {0};

    sscanf(full_string, "text1:%d text2:hello", &value);
    sprintf(listNumber1, "%d", value);
    
    // using for loop and do switch to show each number and calculate number(if list char is not number skip it):
    for(int i = 0; i < strlen(listNumber1); i++)
    {
        switch(listNumber1[i])
        {
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':    
                printf("%c\n", listNumber1[i]);
                result +=  listNumber1[i] - '0';
                break;
            default:
                break;
        }
    }

    return result;
}

int main(void){

    char listNumber[MAX_SIZE_LENGTH] = "text1:1234567 text2:hello";

    int result = calculateString(listNumber);

    // expect resultString show: 28 (1+2+3+4+5+6+7)
    printf("result: %d\n", result);

	return 0;
}
