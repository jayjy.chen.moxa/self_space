#include <stdio.h>
#include <stdlib.h>
/*
 *  
 *  [you may need add library header to use]
 * 
 */


/*
 * compiler: ~# gcc -g q_week1.c -o week1
 * execute:  ~# chmod 777 week1
 *           ~# ./week1
 *
 */

#define MAX_SIZE_LENGTH 16

/****************************************
 * Purpose: Calculate string value into float and store with char string "resultString" inside function
 * Input: two char string
 * Return:
 *        float: result value
 *      
 ****************************************/
float calculateString(char *fromBaseA, char *fromBaseB)
{
    // declare variables if you need:
    float result = 0.0;
    char resultString[MAX_SIZE_LENGTH] = {0};


    // do calculate:



    // store result into string resultString:



    // expect resultString show: 3.01 



    // return calculate result:
    return result;
}

int main(void){

    char baseA[MAX_SIZE_LENGTH] = "1";
    char baseB[MAX_SIZE_LENGTH] = "2.01";

    float result = calculateString(baseA, baseB);

    // expect result print: 3.01 


	return 0;
}
