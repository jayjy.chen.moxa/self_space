#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/*
 *  
 *  [you may need add library header to use]
 * 
 */


#define MAX_SIZE_LENGTH 32

/****************************************
 * Purpose: Storing calculated value from function to buff pointer
 * Input:  int * pointer, char * input string
 *    Return:
 *        0: success
 *       -1: fail
 *      
 ****************************************/
int storeStringFromFunc(int **buff, char *input)
{
    int ret = -1, value = 0;
    char zero = '0';

    // do check input error:
    if(!input)
        return ret;

    // assign memory to buff
    *buff = (int *)malloc(sizeof(int));
    if(buff == NULL)
        return ret;
    
    // initial pointer value
    **buff = 0;
    
    // calculate value
    for(int i = 0; i < strlen(input); i++)
    {
        if(input[i] - (int)zero > 9)
        {
            switch (input[i])
            {
                case 'a':
                case 'b':
                case 'c':
                case 'd':
                case 'e':
                case 'f':
                    value += (int)input[i] - 'a' + 10;
                    break;
                default:
                    ret = -1;
                    return ret;
            }
        }
        else
        {
            value += (int)input[i] - (int)zero;
        }
    }

    // store value to buff
    **buff = value;

    // if success calculated return ret = 0
    ret = 0;
    return ret;
}

int main(void){

    int *buff = NULL;
    char input1[MAX_SIZE_LENGTH] = "9876abcdef54321";
    int ret = -1;

    ret = storeStringFromFunc(&buff, input1);

    // expect result show: 120, 0
    printf("result: %d, ret: %d\n", buff[0], ret);

    // release buff pointer
    if(buff)
        free(buff);

	return 0;
}
