#include <stdio.h>
#include <stdlib.h>
/*
 *  
 *  [you may need add library header to use]
 * 
 */
#include <string.h>

#define MAX_SIZE_LENGTH 128

/****************************************
 * Purpose: Calculate hex map to band value function
 * Input: one char string with band hex value, one char* string array buffer to store band string
 * Return:
 *        integer: band count
 *      
 ****************************************/
int convertBand(char *lte_band, int **band)
{
    int ret = -1;

    // 0. do check if lte_band is NULL or content non-hex value and return -1


    // 1. convert lte_band to binary


    // 2. find bit is 1 with its location number as band number which read from right to left 4 bit (ex: "05" = 0101 = B1, B3)    


    // 3. store band number to variable band


    // 4. calculate total band count and return ret

    return ret;
}

int main(void){
    int *band = NULL;
    int band_count = -1;
    char lte_band[MAX_SIZE_LENGTH] = "00000020080000C5";//full Upper hex char

    band = (int *)calloc(128, sizeof(int));

    band_count = convertBand(lte_band, &band);

    if(band_count < 0)
        return -1;

    // Expect: 1,3,7,8,28,38
    for(int i = 0; i < band_count; i++)
        printf("%d\n", band[i]);

    if(band)
        free(band);
	return 0;
}
