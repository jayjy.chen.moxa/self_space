#include <stdio.h>
#include <stdlib.h>
/*
 *  
 *  [you may need add library header to use]
 * 
 */
#include <string.h>

#define MAX_SIZE_LENGTH 128

/****************************************
 * Purpose: Calculate hex map to band value function
 * Input: one char string with band hex value, one char* string array buffer to store band string
 * Return:
 *        integer: band count
 *      
 ****************************************/
int convertBand(char *lte_band, int **band)
{
    int ret = -1, band_num = 0;
    char zero = '0';

    if(strlen(lte_band) == 0)
        return ret;

    ret = 0;
    // convert lte_band to binary list
    for(int i = strlen(lte_band) - 1, value = 0, bit = 0; i >= 0; i--)
    {
        if(lte_band[i] == zero)
        {
            band_num+=4;
            continue;
        }

        if(lte_band[i] - (int)zero > 9)
        {
            switch (lte_band[i])
            {
                case 'A':
                case 'B':
                case 'C':
                case 'D':
                case 'E':
                case 'F':
                    value += (int)lte_band[i] - 'A' + 10;
                    break;
                default:
                    ret = -1;
                    return ret;
            }
        }
        else
            value += (int)lte_band[i] - (int)zero;

        //run each 4 bit
        for(int count = 0; count < 4; count++) 
        {    
            if(value == 0)//each Band 1~4 are not enable 
            {
                band_num++;
                break;
            }
            band_num++;//start Band 1(1)
            bit = value % 2;

            //match band
            if(bit != 0)
            {
                *(*band+ret) = band_num;
                ret++;
            }
            value = value / 2;//moving right 1 bit
        }
    }
    return ret;
}

int main(void){
    int *band = NULL;
    int band_count = -1;
    char lte_band[MAX_SIZE_LENGTH] = "00000020080000C5";//full Upper

    band = (int *)calloc(128, sizeof(int));

    band_count = convertBand(lte_band, &band);

    if(band_count < 0)
        return -1;

    // Expect: 1,3,7,8,28,38
    for(int i = 0; i < band_count; i++)
        printf("%d\n", band[i]);

    if(band)
        free(band);
	return 0;
}
