// C program for writing
// struct to file
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAGIC_NUMBER "moxa"
#define RELEASE_VERSION "1.1.1" // 1.1.1(_)
#define RELEASE_MODEL_NAME "CCG1500"

/* for compatiable v1.0.0 FW */
struct v1header
{
    int image_1_length;
    char version_name[20];
    char model_name[20];
    int checksum;
};

// a struct to read and write
struct header
{
    char magic_number[4];
    char version_name[16]; // 1.1.1(_)
    char model_name[16];
    int file_count; // not include header
    char reserved[4];
    int checksum;

};

struct subheader
{
    int image_length;
    char filename[20];
    char dst_folder[20];
    char reserved[4];

};

int checksum(char *data, unsigned int size)
{
    int i = 0;
    int sum = 0;
    int value = 0;
    for (i = 0; i <= size - 2; i += 2)
    {
        value = ((int)data[i] << 8) | ((unsigned char)data[i + 1]);
        sum = (sum + value) & 0xffff;
    }

    return sum;
}

/* ./header_gen [file_size] [header_index] [dst_path] */
int main(int argc, char *argv[])
{

    FILE *outfile;
    char filename[20] = {0};

    // open file for writing

    int number = atoi(argv[1]);

    if(number == 0)
    {
        if (argc != 2)
        {
            printf("error header manifest argument\n");
            return -1;
        }
    }
    else
    {
        if (argc != 5)
        {
            printf("error sub header argument\n");
            return -1;
        }
    }

    sprintf(filename, "header%s.dat", argv[1]);
    outfile = fopen(filename, "wb");
    if (outfile == NULL)
    {
        fprintf(stderr, "\nError opened file\n");
        exit(1);
    }

    // header manifest:
    if (number == 0)
    {

        struct header input1 = {MAGIC_NUMBER, RELEASE_VERSION, RELEASE_MODEL_NAME, 2, "", 0}; // 76184829
        int checksum1 = checksum(&input1, sizeof(struct header) - sizeof(int));
        input1.checksum = checksum1;
        // write struct to file
        fwrite(&input1, sizeof(struct header), 1, outfile);

        if (fwrite != 0)
            printf("header manifest contents to file written successfully %d !\n", checksum(&input1, sizeof(struct header) - sizeof(int)));
        else
            printf("error writing file !\n");
    }
    // subheader for file
    else
    {
        struct subheader input2 = {atoi(argv[2]), "update.zip", "dst_path", ""}; // 76184829
        strcpy(input2.filename, argv[4]);
        strcpy(input2.dst_folder, argv[3]);

        // write struct to file
        fwrite(&input2, sizeof(struct subheader), 1, outfile);

        if (fwrite != 0)
            printf("subheader contents to file written successfully!\n");
        else
            printf("error writing file !\n");
    }

    // close file
    fclose(outfile);

    return 0;
}
