import sys
import os
import argparse
import subprocess
from datetime import datetime

now = datetime.now()
current_time_s = datetime.strftime(now,'_BUILD_%Y%m%d%H')

MODEL_NAME= "TR22021101_CONV-0001"

#repo git path
MX_CGI_GIT_PATH="git@gitlab.com:moxa/sw/cnt/CCG/lga_rxl_g1/mxcgi.git"
MX_GPIO_GIT_PATH="git@gitlab.com:moxa/sw/cnt/CCG/lga_rxl_g1/mx_gpio.git"
KERNEL_OEM_GIT_PATH="git@gitlab.com:moxa/sw/cnt/CCG/lga_rxl_g1/kernel_oem.git"
TOOLCHAIN_GIT_PATH="git@gitlab.com:moxa/sw/cnt/CCG/lga_rxl_g1/toolchain.git"
MX_PKAD_GIT_PATH="git@gitlab.com:moxa/sw/cnt/CCG/lga_rxl_g1/mx_pka.git"
MF_API_GIT_PATH="git@gitlab.com:moxa/sw/cnt/CCG/lga_rxl_g1/manufacture_api.git"
BSP_GIT_PATH="git@gitlab.com:moxa/sw/cnt/CCG/lga_rxl_g1/bsp.git"
BSP_BUILD_PATH="/builds/moxa/sw/cnt/CCG/lga_rxl_g1/bsp"

#branch
MX_CGI_BRANCH = "develop"
MX_GPIO_BRANCH = "develop"
KERNEL_OEM_BRANCH = "develop"
TOOLCHAIN_BRANCH = "V1.22.1003"
MX_PKAD_BRANCH = "develop"
MF_API_BRANCH = "develop"
BSP_BRANCH="jay_daily_build"

#working directory
#TOOL_ROOT_DIR="/root/mhqgit/workspace"
TOOL_ROOT_DIR="/tmp/workspace"


#repo directory
MXCGI_DIR = "%s/sdx55-oe-sdk_V1.2.1101/extended/mxcgi111/"%TOOL_ROOT_DIR
MXGPIO_DIR = "%s/sdx55-oe-sdk_V1.2.1101/extended/mxgpio111/"%TOOL_ROOT_DIR
KERNEL_OEM_DIR="%s/kernel_oem"%TOOL_ROOT_DIR
TOOLCHAIN_DIR = "%s/toolchain111/"%TOOL_ROOT_DIR
SDK_DIR = "%s/sdx55-oe-sdk_111/"%TOOL_ROOT_DIR
MXPKAD_DIR = "%s/sdx55-oe-sdk_V1.2.1101/extended/mx_pkad/"%TOOL_ROOT_DIR
MFAPI_DIR = "%s/sdx55-oe-sdk_V1.2.1101/extended/mfapi/"%TOOL_ROOT_DIR
BSP_DIR = "%s/bsp"%TOOL_ROOT_DIR

CEI_OEM_DIR = "%s/sdx55-le-1-0/SDX55_apps/apps_proc/cei-oem"%(KERNEL_OEM_DIR)
CEI_ENV_DIR = "%s/sdx55-le-1-0/ceienv/"%(KERNEL_OEM_DIR)

KERNEL_IMG_OUT_DIR = "%s/out/image"%(CEI_ENV_DIR)
KERNEL_TMP_BUILD_DIR = "%s/sdx55-le-1-0/SDX55_apps/apps_proc/poky/build/tmp-glibc/"%(KERNEL_OEM_DIR)


GEN_FS_TMP_DIR="%s/tmp/ZX52_default"%(CEI_OEM_DIR)

SYSROOT_DIR="/usr/local/oecore-x86_64/sysroots/armv7at2hf-neon-oe-linux-gnueabi/"
#oem folder directory
TARGET_DIR="%s/ZX52_default"%CEI_OEM_DIR
BASE_VERSION_FILE_PATH="%s/base_version"%(TARGET_DIR)


VERSION = "V1.1.1"
BASE_FULL_FOTA_IMAGE_1_FILE_NAME = "full_fota_image_dir/372_update_full_ubi_fota_1.zip"
BASE_FULL_FOTA_IMAGE_2_FILE_NAME = "full_fota_image_dir/372_update_full_ubi_fota_2.zip"

BASE_FULL_IMAGE_FILE_NAME = "RXLG1.20.00.372_0R09.perf.zip"
BASE_FOTA_IMAGE_FILE_NAME = "update.zip"
KERNEL_FILE_NAME = "sdxprairie-boot.img"
OEMFS_FILE_NAME = "sdxprairie-oemfs.ubi "

EXPORT_FULL_IMAGE_FILE_NAME = "%s_%s_full.zip"%(MODEL_NAME, VERSION + current_time_s)
EXPORT_FOTA_IMAGE_FILE_NAME = "%s_%s.rom"%(MODEL_NAME, VERSION + current_time_s)


EXPORT_FULL_FOTA_IMAGE_FILE_1_NAME = "%s_%s_full_1.rom"%(MODEL_NAME, VERSION + current_time_s)
EXPORT_FULL_FOTA_IMAGE_FILE_2_NAME = "%s_%s_full_2.rom"%(MODEL_NAME, VERSION + current_time_s)

EXPORT_FULL_FOTA_IMAGE_FILE_NAME = "%s_%s_full.rom"%(MODEL_NAME, VERSION + current_time_s)

ENCRYPT_PASSWORD = "moxa@89191230@trd"

def system_command(cmd):
    print("\033[92m" + cmd)
    ret = os.system(cmd)
    if(ret):
        sys.exit(ret)

def system_chdir(dir):
    print("DIR:" + dir)
    os.chdir(dir)

def sdk_enviornment_prepare():
    #download cache of bitbake for kernel building
    #system_chdir(CEI_ENV_DIR)
    #system_command("git config --global user.email \"you@example.com\"")
    #system_command("git config --global user.name \"Your Name\"")
    #build cache 1 
    #system_command("git cherry-pick 2720cfff0bf86a4c366202018dd47992c740e5ae > /dev/null")
    #build cache 2 
    #system_command("git cherry-pick 9caccffb2d37d8c2ba0f988e31a3a6a14fe8d043 > /dev/null")
    #fix build error patch 
    #system_command("git cherry-pick cae3312bd7c05aa31e26d3941e4324358d5e1080 > /dev/null")
    #cp current defconfig to workdir
    #system_chdir(KERNEL_OEM_DIR)
    #system_command("cp sdx55-le-1-0/SDX55_apps/apps_proc/kernel/msm-4.14/arch/arm/configs/vendor/sdxprairie_zx52-perf_defconfig sdx55-le-1-0/SDX55_apps/apps_proc/poky/build/tmp-glibc/work/sdxprairie-oe-linux-gnueabi/linux-msm/4.14-r0/defconfig")
 
    #install sdk to /usr/local/oecore-x86_64
    system_chdir(TOOLCHAIN_DIR)
    system_command("mkdir -p %s" %(SDK_DIR))
    system_command("tar xvf sdx55-oe-sdk.tar.gz -C %s --strip-components=1"%SDK_DIR)
    system_chdir(SDK_DIR)

    command = "./oecore-x86_64-armv7at2hf-neon-toolchain-nodistro.0.sh"
    p = subprocess.Popen(command, stdin=subprocess.PIPE, shell=True)
    p.communicate(input=("\ny\n".encode()))
    
    #install cri-client lib and header
    system_command("mkdir -p %s/usr/include/cri-client "%SYSROOT_DIR)
    system_command("cp -r %s/extended/cri-client/armv7at2hf-neon-oe-linux-gnueabi/usr/include/cri-client/* %s/usr/include/cri-client/ "%(SDK_DIR, SYSROOT_DIR))
    system_command("cp -r %s/extended/cri-client/armv7at2hf-neon-oe-linux-gnueabi/usr/lib/* %s/usr/lib/ "%(SDK_DIR, SYSROOT_DIR))
    #install openssl lib and header
    system_command("mkdir -p %s/usr/include/openssl "%SYSROOT_DIR)
    system_command("cp -r %s/extended/openssl/armv7at2hf-neon-oe-linux-gnueabi/usr/include/openssl/* %s/usr/include/openssl/ "%(SDK_DIR, SYSROOT_DIR))
    system_command("cp -r %s/extended/openssl/armv7at2hf-neon-oe-linux-gnueabi/usr/lib/* %s/usr/lib/ "%(SDK_DIR, SYSROOT_DIR))
    #install libxcrypto lib and header
    system_command("mkdir -p %s/usr/include/"%SYSROOT_DIR)
    system_command("cp -r %s/extended/libxcrypt/armv7at2hf-neon-oe-linux-gnueabi/usr/include/* %s/usr/include/ "%(SDK_DIR, SYSROOT_DIR))
    system_command("cp -r %s/extended/libxcrypt/armv7at2hf-neon-oe-linux-gnueabi/usr/lib/* %s/usr/lib/ "%(SDK_DIR, SYSROOT_DIR))
    #install libmf header
    system_command("mkdir -p %s/usr/include/"%SYSROOT_DIR)
    system_command("cp -r %s/include/* %s/usr/include/ "%(MFAPI_DIR, SYSROOT_DIR))
    
    system_command("cp %s/%s %s/%s"%(TOOLCHAIN_DIR, KERNEL_FILE_NAME, TOOL_ROOT_DIR, "boot.img"))

def build():
    print(TOOL_ROOT_DIR)
    system_command("ls")
    #build mfapi
    system_chdir(MFAPI_DIR)
    system_command(". /usr/local/oecore-x86_64/environment-setup-armv7at2hf-neon-oe-linux-gnueabi; make;")
    system_command("TARGET_DIR=%s make install;"% TARGET_DIR)
    #install libmf lib for another service
    system_command("TARGET_DIR=%s make install;"% SYSROOT_DIR)
    #build mxcgi
    system_chdir(MXCGI_DIR)
    system_command(". /usr/local/oecore-x86_64/environment-setup-armv7at2hf-neon-oe-linux-gnueabi; make;")
    system_command("TARGET_DIR=%s make install;"% TARGET_DIR)
    #build mxgpio
    system_chdir(MXGPIO_DIR)
    system_command(". /usr/local/oecore-x86_64/environment-setup-armv7at2hf-neon-oe-linux-gnueabi; make;")
    system_command("TARGET_DIR=%s make install;"% TARGET_DIR)
    #build mx_pkad
    system_chdir(MXPKAD_DIR)
    system_command(". /usr/local/oecore-x86_64/environment-setup-armv7at2hf-neon-oe-linux-gnueabi; make;")
    system_command("TARGET_DIR=%s make install;"% TARGET_DIR)
    #create version
    system_command("echo \"%s\" > %s/data/MOXA_FIRMWARE_VERSION "% (VERSION + current_time_s, TARGET_DIR))
    
    system_chdir(CEI_OEM_DIR)
    #gen ubi image
    system_command("./generate_oem_ubi.sh ZX52_default;")
    system_chdir(TOOL_ROOT_DIR)
    #create fota image
    system_command("cp %s/%s %s/%s"%(TOOLCHAIN_DIR, BASE_FOTA_IMAGE_FILE_NAME, TOOL_ROOT_DIR, BASE_FOTA_IMAGE_FILE_NAME))

    system_command("mkdir -p %s/firmware-update"% TOOL_ROOT_DIR)
    system_command("cp %s/%s  %s/firmware-update/"% (GEN_FS_TMP_DIR, OEMFS_FILE_NAME, TOOL_ROOT_DIR))
    system_command("cp %s  %s/"% (BASE_VERSION_FILE_PATH, TOOL_ROOT_DIR))
    system_command("zip ./%s firmware-update/%s"%(BASE_FOTA_IMAGE_FILE_NAME, OEMFS_FILE_NAME))
    system_command("zip ./%s ./%s"%(BASE_FOTA_IMAGE_FILE_NAME, "boot.img"))
    system_command("zip %s update.zip base_version -P %s"%(EXPORT_FOTA_IMAGE_FILE_NAME, ENCRYPT_PASSWORD))

def build_full():
    print(TOOL_ROOT_DIR)
    system_command("ls")
    #build mfapi
    system_chdir(MFAPI_DIR)
    system_command(". /usr/local/oecore-x86_64/environment-setup-armv7at2hf-neon-oe-linux-gnueabi; make;")
    system_command("TARGET_DIR=%s make install;"% TARGET_DIR)
    #install libmf lib for another service
    system_command("TARGET_DIR=%s make install;"% SYSROOT_DIR)
    #build mxcgi
    system_chdir(MXCGI_DIR)
    system_command(". /usr/local/oecore-x86_64/environment-setup-armv7at2hf-neon-oe-linux-gnueabi; make;")
    system_command("TARGET_DIR=%s make install;"% TARGET_DIR)
    #build mxgpio
    system_chdir(MXGPIO_DIR)
    system_command(". /usr/local/oecore-x86_64/environment-setup-armv7at2hf-neon-oe-linux-gnueabi; make;")
    system_command("TARGET_DIR=%s make install;"% TARGET_DIR)
    #build mx_pkad
    system_chdir(MXPKAD_DIR)
    system_command(". /usr/local/oecore-x86_64/environment-setup-armv7at2hf-neon-oe-linux-gnueabi; make;")
    system_command("TARGET_DIR=%s make install;"% TARGET_DIR)
    #create version
    system_command("echo %s > %s/data/MOXA_FIRMWARE_VERSION "% (VERSION + current_time_s, TARGET_DIR))
    system_chdir(CEI_OEM_DIR)
    #gen ubi image
    system_command("./generate_oem_ubi.sh ZX52_default;")
    system_chdir(TOOL_ROOT_DIR)
    #create full image
    system_command("cp %s/%s %s/%s"%(TOOLCHAIN_DIR, BASE_FULL_IMAGE_FILE_NAME, TOOL_ROOT_DIR, EXPORT_FULL_IMAGE_FILE_NAME))
    system_command("cp %s/%s  %s/%s"% (GEN_FS_TMP_DIR, OEMFS_FILE_NAME, TOOL_ROOT_DIR, OEMFS_FILE_NAME))
    system_command("zip ./%s ./%s"%(EXPORT_FULL_IMAGE_FILE_NAME, OEMFS_FILE_NAME))
    system_command("cp ./boot.img  ./sdxprairie-boot.img")
    system_command("zip ./%s ./%s"%(EXPORT_FULL_IMAGE_FILE_NAME, "sdxprairie-boot.img"))

def build_full_fota():
    print(TOOL_ROOT_DIR)
    system_command("ls")
    #build mfapi
    system_chdir(MFAPI_DIR)
    system_command(". /usr/local/oecore-x86_64/environment-setup-armv7at2hf-neon-oe-linux-gnueabi; make;")
    system_command("TARGET_DIR=%s make install;"% TARGET_DIR)
    #install libmf lib for another service
    system_command("TARGET_DIR=%s make install;"% SYSROOT_DIR)
    #build mxcgi
    system_chdir(MXCGI_DIR)
    system_command(". /usr/local/oecore-x86_64/environment-setup-armv7at2hf-neon-oe-linux-gnueabi; make;")
    system_command("TARGET_DIR=%s make install;"% TARGET_DIR)
    #build mxgpio
    system_chdir(MXGPIO_DIR)
    system_command(". /usr/local/oecore-x86_64/environment-setup-armv7at2hf-neon-oe-linux-gnueabi; make;")
    system_command("TARGET_DIR=%s make install;"% TARGET_DIR)
    #build mx_pkad
    system_chdir(MXPKAD_DIR)
    system_command(". /usr/local/oecore-x86_64/environment-setup-armv7at2hf-neon-oe-linux-gnueabi; make;")
    system_command("TARGET_DIR=%s make install;"% TARGET_DIR)
    #create version
    system_command("echo %s > %s/data/MOXA_FIRMWARE_VERSION "% (VERSION + current_time_s, TARGET_DIR))
    system_chdir(CEI_OEM_DIR)
    #gen ubi image
    system_command("./generate_oem_ubi.sh ZX52_default;")
    
    system_chdir(TOOL_ROOT_DIR)

    #create full fota image 1 (sysfs and kernel)
    
    system_command("cp %s/%s %s/%s"%(TOOLCHAIN_DIR, BASE_FULL_FOTA_IMAGE_1_FILE_NAME, TOOL_ROOT_DIR, BASE_FOTA_IMAGE_FILE_NAME))

    system_command("echo %s > ./FIRMWARE_VERSION "% (VERSION + current_time_s))
    system_command("touch ./IMAGE_TYPE_ROM_1")

    system_command("zip ./%s ./%s"%(BASE_FOTA_IMAGE_FILE_NAME, "boot.img"))

    system_command("zip %s update.zip ./FIRMWARE_VERSION ./IMAGE_TYPE_ROM_1 -P %s"%(EXPORT_FULL_FOTA_IMAGE_FILE_1_NAME, ENCRYPT_PASSWORD))



    #create full fota image 2 (HLOS and oem)
    
    system_command("cp %s/%s %s/%s"%(TOOLCHAIN_DIR, BASE_FULL_FOTA_IMAGE_2_FILE_NAME, TOOL_ROOT_DIR, BASE_FOTA_IMAGE_FILE_NAME))

    system_command("mkdir -p %s/firmware-update"% TOOL_ROOT_DIR)
    system_command("cp %s/%s  %s/firmware-update/"% (GEN_FS_TMP_DIR, OEMFS_FILE_NAME, TOOL_ROOT_DIR))
    
    system_command("echo %s > ./FIRMWARE_VERSION "% (VERSION + current_time_s))
    system_command("touch ./IMAGE_TYPE_ROM_2")
    system_command("zip ./%s firmware-update/%s"%(BASE_FOTA_IMAGE_FILE_NAME, OEMFS_FILE_NAME))

    system_command("zip %s update.zip ./FIRMWARE_VERSION ./IMAGE_TYPE_ROM_2 -P %s"%(EXPORT_FULL_FOTA_IMAGE_FILE_2_NAME, ENCRYPT_PASSWORD))

    #gen full fota header
    system_command("cp %s/header_gen.c ."%BSP_BUILD_PATH)
    system_command("gcc header_gen.c -o header_gen")
    system_command("./header_gen 0")
    system_command("./header_gen 1 %d /tmp upload_file"%(os.stat(EXPORT_FULL_FOTA_IMAGE_FILE_2_NAME).st_size))
    system_command("./header_gen 2 %d /data upload_file2"%(os.stat(EXPORT_FULL_FOTA_IMAGE_FILE_1_NAME).st_size))
    system_command("cat header1.dat >> header0.dat")
    system_command("cat %s >> header0.dat"%EXPORT_FULL_FOTA_IMAGE_FILE_2_NAME)
    system_command("cat header2.dat >> header0.dat")
    system_command("cat %s >> header0.dat"%EXPORT_FULL_FOTA_IMAGE_FILE_1_NAME)
    system_command("mv header0.dat %s"%EXPORT_FULL_FOTA_IMAGE_FILE_NAME)
    
def build_kernel():
    system_chdir(CEI_ENV_DIR)
    #system_command("BBPATH=\"/tmp/workspace/kernel_oem/sdx55-le-1-0/SDX55_apps/apps_proc/poky/meta/conf/\" bash -c \"source ./zx52_env.sh; make apps_kernel_perf;\"")
    system_command("source ./zx52_env.sh; make apps_kernel_perf;")
    system_command("cp %s/%s %s/%s"%(KERNEL_IMG_OUT_DIR, KERNEL_FILE_NAME, TOOL_ROOT_DIR, "boot.img"))
    system_chdir(KERNEL_OEM_DIR)
    system_command("cat sdx55-le-1-0/SDX55_apps/apps_proc/poky/build/tmp-glibc/work/sdxprairie-oe-linux-gnueabi/linux-msm/4.14-r0/defconfig")

    
def sync_git_repo():
    #sync repo
    system_chdir(MXCGI_DIR)
    system_command("git pull")
    system_chdir(MXGPIO_DIR)
    system_command("git pull")
    system_chdir(KERNEL_OEM_DIR)
    system_command("git pull")
    system_chdir(TOOLCHAIN_DIR)
    system_command("git pull")
    system_chdir(MXPKAD_DIR)
    system_command("git pull")
    system_chdir(MFAPI_DIR)
    system_command("git pull")
    system_chdir(BSP_DIR)
    system_command("git pull")

    print("you have to run prepare again.");
    
def clean_repo():
    #sync repo
    system_chdir(MXCGI_DIR)
    system_command("make clean")
    system_chdir(MXGPIO_DIR)
    system_command("make clean")
    system_chdir(MXPKAD_DIR)
    system_command("make clean")
    system_chdir(MFAPI_DIR)
    system_command("make clean")
    system_chdir(BSP_DIR)
    system_command("make clean")

    system_command("rm -rf %s"%KERNEL_TMP_BUILD_DIR)
def clone_git_repo():
    #build mxcgi
    system_chdir(TOOL_ROOT_DIR)
    if(os.path.exists(TOOLCHAIN_DIR) == False):
        system_command("mkdir -p %s"%(TOOLCHAIN_DIR))
        system_command("git clone -b %s %s %s" %(TOOLCHAIN_BRANCH, TOOLCHAIN_GIT_PATH, TOOLCHAIN_DIR))
    if(os.path.exists(MXCGI_DIR) == False):
        system_command("mkdir -p %s"%(MXCGI_DIR))
        system_command("git clone -b %s %s %s" %(MX_CGI_BRANCH ,MX_CGI_GIT_PATH, MXCGI_DIR))
    if(os.path.exists(KERNEL_OEM_DIR)  == False):
        system_command("mkdir -p %s"%(KERNEL_OEM_DIR))
        system_command("git clone -b %s %s %s" %(KERNEL_OEM_BRANCH, KERNEL_OEM_GIT_PATH, KERNEL_OEM_DIR))
    if(os.path.exists(MXGPIO_DIR) == False):
        system_command("mkdir -p %s"%(MXGPIO_DIR))
        system_command("git clone -b %s %s %s" %(MX_GPIO_BRANCH ,MX_GPIO_GIT_PATH, MXGPIO_DIR))
    if(os.path.exists(MXPKAD_DIR) == False):
        system_command("mkdir -p %s"%(MXPKAD_DIR))
        system_command("git clone -b %s %s %s" %(MX_PKAD_BRANCH ,MX_PKAD_GIT_PATH, MXPKAD_DIR))
    if(os.path.exists(MFAPI_DIR) == False):
        system_command("mkdir -p %s"%(MFAPI_DIR))
        system_command("git clone -b %s %s %s" %(MF_API_BRANCH ,MF_API_GIT_PATH, MFAPI_DIR))
    if(os.path.exists(BSP_DIR) == False):
        system_command("mkdir -p %s"%(BSP_DIR))
        system_command("git clone -b %s %s %s" %(BSP_BRANCH ,BSP_GIT_PATH, BSP_DIR))

def tag_git_repo(tag_name):
    #tag repo
    system_chdir(MXCGI_DIR)
    system_command("git tag %s"%tag_name)
    system_chdir(MXGPIO_DIR)
    system_command("git tag %s"%tag_name)
    system_chdir(KERNEL_OEM_DIR)
    system_command("git tag %s"%tag_name)
    system_chdir(TOOLCHAIN_DIR)
    system_command("git tag %s"%tag_name)
    system_chdir(MXPKAD_DIR)
    system_command("git tag %s"%tag_name)
    system_chdir(MFAPI_DIR)
    system_command("git tag %s"%tag_name)
    system_chdir(BSP_DIR)
    system_command("git tag %s"%tag_name)

def push_tag_git_repo(tag_name):
    #tag repo
    system_chdir(MXCGI_DIR)
    system_command("git push origin %s"%tag_name)
    system_chdir(MXGPIO_DIR)
    system_command("git push origin %s"%tag_name)
    system_chdir(KERNEL_OEM_DIR)
    system_command("git push origin %s"%tag_name)
    system_chdir(TOOLCHAIN_DIR)
    system_command("git push origin %s"%tag_name)
    system_chdir(MXPKAD_DIR)
    system_command("git push origin %s"%tag_name)
    system_chdir(MFAPI_DIR)
    system_command("git push origin %s"%tag_name)
    system_chdir(BSP_DIR)
    system_command("git push origin %s"%tag_name)

def show_branch_repo():
    #tag repo
    system_chdir(MXCGI_DIR)
    system_command("git branch")
    system_chdir(MXGPIO_DIR)
    system_command("git branch")
    system_chdir(KERNEL_OEM_DIR)
    system_command("git branch")
    system_chdir(TOOLCHAIN_DIR)
    system_command("git branch")
    system_chdir(MXPKAD_DIR)
    system_command("git branch")
    system_chdir(MFAPI_DIR)
    system_command("git branch")
    system_chdir(BSP_DIR)
    system_command("git branch")

def checkout_default_branch_repo():
    #tag repo
    system_chdir(MXCGI_DIR)
    system_command("git checkout %s"%MX_CGI_BRANCH)
    system_chdir(MXGPIO_DIR)
    system_command("git checkout %s"%MX_GPIO_BRANCH)
    system_chdir(KERNEL_OEM_DIR)
    system_command("git checkout %s"%KERNEL_OEM_BRANCH)
    system_chdir(TOOLCHAIN_DIR)
    system_command("git checkout %s"%TOOLCHAIN_BRANCH)
    system_chdir(MXPKAD_DIR)
    system_command("git checkout %s"%MX_PKAD_BRANCH)
    system_chdir(MFAPI_DIR)
    system_command("git checkout %s"%MX_PKAD_BRANCH)
    system_chdir(BSP_DIR)
    system_command("git checkout %s"%BSP_BRANCH)

def main(args = sys.argv[1:]):
    parser = argparse.ArgumentParser()

    # Allow options, but not together.
    group = parser.add_argument_group()
    group.add_argument("--build",
                        help = "build fota image",
                        action = "store_true")
    group.add_argument("--prepare",
                        help = "prepare sdk",
                        action = "store_true")
    group.add_argument("--clone",
                        help = "git clone repos",
                        action = "store_true")
    group.add_argument("--clean",
                        help = "clean git repo",
                        action = "store_true")
    group.add_argument("--sync",
                        help = "sync git repo",
                        action = "store_true")
    group.add_argument("--build_full",
                        help = "build full image",
                        action = "store_true")
    group.add_argument("--build_full_fota",
                        help = "build full fota",
                        action = "store_true")
    group.add_argument("--build_kernel",
                        help = "build kernel",
                        action = "store_true")
    group.add_argument("--tag",
                        help = "tag git repo", type=str)
    group.add_argument("--push_tag",
                        help = "push tag", type=str)
    group.add_argument("--show_branch",
                        help = "show current branch",
                        action = "store_true")
    group.add_argument("--checkout",
                        help = "checkout default branch",
                        action = "store_true")


    args = parser.parse_args(args)
    
    if(os.path.exists(TOOL_ROOT_DIR) == False):
        system_command("mkdir -p %s"%(TOOL_ROOT_DIR))

    if args.build:
        build()
    elif args.prepare:
        sdk_enviornment_prepare()
    elif args.clone:
        clone_git_repo()
    elif args.sync:
        sync_git_repo()
    elif args.clean:
        clean_repo()
    elif args.build_full:
        build_full()
    elif args.build_full_fota:
        build_full_fota()
    elif args.build_kernel:
        build_kernel()
    elif args.tag:
        tag_git_repo(args.tag)
    elif args.push_tag:
        push_tag_git_repo(args.push_tag)
    elif args.push_tag:
        push_tag_git_repo(args.push_tag)
    elif args.show_branch:
        show_branch_repo()
    elif args.checkout:
        checkout_default_branch_repo()
    else:
        print("wrong arguments");

if __name__ == '__main__':
    main()
