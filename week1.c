#include <stdio.h>
#include <stdlib.h>
/*
 *  
 *  [you may need add library header to use]
 * 
 */


#define MAX_SIZE_LENGTH 16

/****************************************
 * Purpose: Calculate string value into float and store with char string "resultString" inside function
 * Input: two char string
 * Return:
 *        int: result value
 *      
 ****************************************/
float calculateString(char *fromBaseA, char *fromBaseB)
{
    float baseA = 0.0, baseB = 0.0;
    float result = 0.0;
    char resultString[MAX_SIZE_LENGTH] = {0};

    // do calculate:
    sscanf(fromBaseA, "%f", &baseA);
    sscanf(fromBaseB, "%f", &baseB);
    result = baseA + baseB;

    // store result into string resultString:
    sprintf(resultString, "%0.2f", result);


    // expect resultString show: 3.01 
    printf("string: %s\n", resultString);

    // return calculate result:
    return result;
}

int main(void){

    char baseA[MAX_SIZE_LENGTH] = "1";
    char baseB[MAX_SIZE_LENGTH] = "2.01";

    float result = calculateString(baseA, baseB);

    // expect result show: 3.01 
    printf("float: %.2f\n", result);

	return 0;
}
