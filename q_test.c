#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/*
 *  
 *  [you may need add library header to use]
 * 
 */
#define MAX_SIZE_LENGTH 32

/****************************************
 * Purpose: Recursion function to get member list
 * Input:  int full list array, int full list size, int remain value, int current index for full list, int member list, int last result count
 *    Return:
 *        int value: all combination count
 *      
 ****************************************/
int recursion_get_sets(int list[], int input_size, int sum, int index, int* member_list, int member_size, int count) {

   if (sum == 0) 
   {
       count++;
       return count;
   }
   for (int i = index; i < input_size; i++) {
       if (list[i] <= sum) {
           member_list[member_size] = list[i];
           count = recursion_get_sets(list, input_size, sum - list[i], i, member_list, member_size + 1, count);
       }
   }
   return count;
}

/****************************************
 * Purpose: Find Combination Sum
 * Input:  int list array, list elements, target sum
 *    Return:
 *        int value: all combination count
 *      
 ****************************************/
int calculate_combination_func(int list[], int input_size, int sum) {
   int member_list[100];
   int count = 0;

   count = recursion_get_sets(list, input_size, sum, 0, member_list, 0, count);
   return count;
}

int main(void) {
    int list[4] = {2,3,5,8};
    int input_size = 4;
    int sum = 7;
    int combination_count = 0;
    
    combination_count = calculate_combination_func(list, input_size, sum);

    // expect show all combination count: 2 ([2,2,3], [2,5])
    printf("all input combination count: %d\n", combination_count);
    return 0;
}