#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int read_kernel_time(char **buff)
{
    int ret = -1;
    int i = 0;
    FILE *fp = NULL;
    char tmp[128] = {0};
    char *sub_string = NULL;

    fp = popen("uname -a", "r");
    if(!fp)
        return ret;
    
    while(fgets(tmp, 128, fp) != NULL)
    {
        sub_string = strtok(tmp, " ");
        do
        {
            if(i == 8)
            {
                memcpy(*buff, sub_string, strlen(sub_string));
                break;
            }
            sub_string = strtok(NULL, " ");
            i++;
        } while(sub_string);
        memset(tmp, 0, 128);
    }

    if(fp)
        fclose(fp);

    ret = 0;
    return ret;
}

int write_kernel_time(char *buff)
{
    int ret = -1;
    FILE *fp = NULL;
    fp = fopen("build_kernel_time.txt", "wt+");
    if(!fp)
        return ret;

    fwrite(buff, 1, strlen(buff), fp);

    if(fp)
        fclose(fp);
    ret = 0;
    return ret;
}

int main(void){

    char *buff = NULL;
    buff = (char *)malloc(sizeof(char) * 8);
    if(buff == NULL)
        return -1;

    //1. using popen "uname -r" read build kernel time like: "01:23:45" and store to buff
    if(read_kernel_time(&buff) < 0)
        return -1;

    //2. write buff value to file with name is "build_kernel_time.txt"
    if(write_kernel_time(buff) < 0)
        return -1;

    if(buff)
        free(buff);

	return 0;
}
